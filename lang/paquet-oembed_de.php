<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-oembed?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// O
	'oembed_description' => 'oEmbed Client/Provider für SPIP.',
	'oembed_slogan' => 'oEmbed Client/Provider für SPIP',
];
