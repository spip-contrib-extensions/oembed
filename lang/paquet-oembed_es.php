<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-oembed?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// O
	'oembed_description' => 'oEmbed cliente/proveedor para SPIP.',
	'oembed_slogan' => 'oEmbed cliente/proveedor para SPIP',
];
