<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-oembed?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// O
	'oembed_description' => 'oEmbed client/provider per SPIP.',
	'oembed_slogan' => 'oEmbed client/provider per SPIP',
];
