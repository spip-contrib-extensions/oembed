<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-oembed?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// O
	'oembed_description' => 'Klient/poskytovateľ oEmbedu pre SPIP.',
	'oembed_slogan' => 'Klient/poskytovateľ oEmbedu pre SPIP',
];
